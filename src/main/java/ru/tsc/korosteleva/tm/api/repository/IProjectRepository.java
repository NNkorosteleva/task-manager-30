package ru.tsc.korosteleva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Project create(@NotNull String userId,
                   @NotNull String name,
                   @NotNull String description,
                   @NotNull Date dateBegin,
                   @NotNull Date dateEnd);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project updateById(@NotNull String userId,
                       @NotNull String id,
                       @NotNull String name,
                       @NotNull String description);

    @Nullable
    Project updateByIndex(@NotNull String userId,
                          @NotNull Integer index,
                          @NotNull String name,
                          @NotNull String description);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Project changeProjectStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

}