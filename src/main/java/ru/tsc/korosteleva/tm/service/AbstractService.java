package ru.tsc.korosteleva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.repository.IRepository;
import ru.tsc.korosteleva.tm.api.service.IService;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.exception.entity.ModelNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.IdEmptyException;
import ru.tsc.korosteleva.tm.exception.field.IndexIncorrectException;
import ru.tsc.korosteleva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.add(model);

    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model.orElseThrow(ModelNotFoundException::new);

    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize())
                .orElseThrow(IndexIncorrectException::new);
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.remove(model);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional<M> model = Optional.ofNullable(repository.removeById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize())
                .orElseThrow(IndexIncorrectException::new);
        Optional<M> model = Optional.ofNullable(repository.removeByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public void removeAll(final @Nullable  Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return repository.existsById(id);
    }

}
