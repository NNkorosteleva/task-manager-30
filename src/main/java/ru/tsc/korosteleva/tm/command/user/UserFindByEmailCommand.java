package ru.tsc.korosteleva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserFindByEmailCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-find-by-email";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Find user by email.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FIND USER BY EMAIL]");
        System.out.println("[ENTER EMAIL:]");
        String email = TerminalUtil.nextLine();
        final User user = getUserService().findOneByEmail(email);
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
