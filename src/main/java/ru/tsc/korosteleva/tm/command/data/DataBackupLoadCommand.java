package ru.tsc.korosteleva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.dto.Domain;
import ru.tsc.korosteleva.tm.enumerated.Role;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Load backup from base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        final boolean isBackupExist = Files.exists(Paths.get(FILE_BACKUP));
        if (!isBackupExist) return;
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

